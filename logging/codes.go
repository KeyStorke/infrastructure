package logging

type EventCode int64

// Default Levels priorities
const (
	ERROR = 0x00
	INFO  = 0x10
)

// Default events codes
const (
	COMMENCE = 0x00
	COMPLETE = 0x10
)
