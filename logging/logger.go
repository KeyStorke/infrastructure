package logging

import (
	"fmt"
	"strconv"
	"strings"
	"time"
	"sync"
	"io"
	"errors"
)

const (
	FUNCNAME = "%FUNCNAME%" // name of the function
	LINENO   = "%LINENO%"   // number of line code
	FILENAME = "%FILENAME%" // name of file
	FILEPATH = "%FILEPATH%" // full path to file
	MSECOND	 = "%MSECOND%"	// millisecond
	SECOND	 = "%SECOND%"	// second
	MINUTE	 = "%MINUTE%"	// minute
	HOUR	 = "%HOUR%"	    // hour
	DAY		 = "%DAY%"		// number of day
	MONTH	 = "%MONTH%"	// number of month
	YEAR	 = "%YEAR%"		// number of year

	DefaultLoggerName = "default"
)


var Loggers = NewLoggerMap()

type Logger struct {
	events EventsMap
	levels LevelsMap
	name   string
	sync   sync.Mutex
}

// public functions

func NewLogger(name string) (logger *Logger) {
	logger = &Logger{
		name:   name,
		levels: NewLevelsMap(),
		events: NewEventMap(),
		sync:	sync.Mutex{},
	}

	Loggers.Set(name, logger)
	return logger
}

func GetDefaultLogger() (logger Logger, err error) {
	return GetLogger(DefaultLoggerName)
}

func GetLogger(name string) (logger Logger, err error) {

	if !Loggers.Exists(name) {
		errMsg := fmt.Sprintf("%s logger not initialized", name)
		fmt.Println(errMsg)
		return logger, errors.New(errMsg)
	}

	logger = *Loggers.Get(name)
	return logger, err
}

func CloneDefaultLogger (newName string) (logger Logger) {
	logger, err := GetDefaultLogger()

	if err != nil {
		return
	}

	logger.name = newName
	Loggers.Set(newName, &logger)
	return logger
}

func SetOutput (loggerName string, priority Priority, writer io.Writer) {
	logger, err := GetLogger(loggerName)

	if err != nil {
		return
	}

	level, err := logger.getLevel(priority)

	if err != nil {
		fmt.Println(err)
		return
	}

	level.Output = writer
	logger.AddLevel(*level)

}

func LogEventWithLogger(loggerName string, code EventCode, artifacts ...interface{}) {

	if !Loggers.Exists(loggerName) {
		errMsg := "Logger %s not found"
		errMsg = fmt.Sprintf(errMsg, loggerName)
		fmt.Println(errMsg)
		return
	}

	Loggers.Get(loggerName).LogEvent(code, artifacts...)
}

func LogEvent(code EventCode, artifacts ...interface{}) {
	logger, err := GetDefaultLogger()

	if err != nil {
		return
	}

	logger.LogEvent(code, artifacts...)
}

func AddEvent (event Event) {
	logger, err := GetDefaultLogger()

	if err != nil {
		return
	}

	logger.AddEvent(event)
}

func SetDefaultOutput(priority Priority, writer io.Writer) {
	SetOutput(DefaultLoggerName, priority, writer)
}

// public methods

func (l *Logger) AddEvent(event Event) {
	l.sync.Lock()
	defer l.sync.Unlock()

	l.events.AddEvent(event)
}

func (l *Logger) AddLevel(level Level) {
	l.sync.Lock()
	defer l.sync.Unlock()

	l.levels.AddLevel(level)
}

func (l *Logger) LogEvent(code EventCode, artifacts ...interface{}) {
	l.sync.Lock()
	defer l.sync.Unlock()

	event, err := l.events.GetEvent(code)

	if failed(err) {
		fmt.Println(err)
		return
	}

	level, err := l.levels.GetLevel(event.Priority)

	if failed(err) {
		fmt.Println(err)
		return
	}

	message := formatMessage(artifacts, level.Suffix, event)
	_, err = level.Output.Write(message)

	if failed(err) {
		fmt.Println("failed")
		fmt.Println(err)
	}
}

// private methods

func (l *Logger) getEvent(code EventCode) (event *Event, err error) {
	event, err = l.events.GetEvent(code)
	return event, err
}

func (l *Logger) getLevel(priority Priority) (level *Level, err error) {
	level, err = l.levels.GetLevel(priority)
	return level, err
}

// private functions

func formatMessageArtifacts(msg string) string {

	now := time.Now()

	if strings.Contains(msg, FUNCNAME) {
		msg = strings.Replace(msg, FUNCNAME, FuncName(), -1)
	}

	if strings.Contains(msg, FILENAME) {
		msg = strings.Replace(msg, FILENAME, Filename(), -1)
	}

	if strings.Contains(msg, LINENO) {
		msg = strings.Replace(msg, LINENO, strconv.Itoa(Line()), -1)
	}

	if strings.Contains(msg, FILEPATH) {
		msg = strings.Replace(msg, FILEPATH, Filepath(), -1)
	}

	if strings.Contains(msg, MSECOND) {
		msec := strconv.Itoa(now.Nanosecond() / int(time.Millisecond))
		msg = strings.Replace(msg, MSECOND, msec, -1)
	}

	if strings.Contains(msg, SECOND) {
		second := strconv.Itoa(now.Second())
		msg = strings.Replace(msg, SECOND, second, -1)
	}

	if strings.Contains(msg, MINUTE) {
		minute := strconv.Itoa(now.Minute())
		msg = strings.Replace(msg, MINUTE, minute, -1)
	}
	//
	if strings.Contains(msg, HOUR) {
		hour := strconv.Itoa(now.Hour())
		msg = strings.Replace(msg, HOUR, hour, -1)
	}

	if strings.Contains(msg, DAY) {
		day := strconv.Itoa(now.Day())
		msg = strings.Replace(msg, DAY, day, -1)
	}

	if strings.Contains(msg, MONTH) {
		month := now.Month().String()
		msg = strings.Replace(msg, MONTH, month, -1)
	}

	if strings.Contains(msg, YEAR) {
		year := strconv.Itoa(now.Year())
		msg = strings.Replace(msg, YEAR, year, -1)
	}

	return msg
}

func formatMessage(artifacts []interface{}, suffix string, event *Event) []byte {
	var msg string

	suffix = formatMessageArtifacts(suffix)
	msg = formatMessageArtifacts(event.Message)

	msg = fmt.Sprintf(msg, artifacts...)
	msg = fmt.Sprintf("%s %s", suffix, msg)
	msg = fmt.Sprintln("", msg)

	return []byte(msg)
}

func failed(err error) (failed bool) {
	return err != nil
}

func init() {
	defaultLogger := NewLogger(DefaultLoggerName)
	defaultLogger.AddLevel(InfoLevel)
	defaultLogger.AddLevel(ErrorLevel)
	defaultLogger.AddEvent(CommenceEvent)
	defaultLogger.AddEvent(CompleteEvent)
}
