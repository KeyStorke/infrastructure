package logging

import "sync"

type LoggersMap struct {
	loggers 		map[string]Logger
	loggersPointers map[string]*Logger
	sync 			sync.Mutex
}

func NewLoggerMap () (loggersMap LoggersMap) {

	loggersMap = LoggersMap{
		loggers:         make(map[string]Logger),
		loggersPointers: make(map[string]*Logger),
		sync:			 sync.Mutex{},
	}

	return loggersMap
}

func (m *LoggersMap) Set (name string, logger *Logger) {
	m.sync.Lock()
	defer m.sync.Unlock()

	m.loggers[name] = *logger
	m.loggersPointers[name] = logger
}

func (m *LoggersMap) Get (name string) (logger *Logger) {
	m.sync.Lock()
	defer m.sync.Unlock()

	return m.loggersPointers[name]
}

func (m *LoggersMap) Exists (name string) bool {
	m.sync.Lock()
	defer m.sync.Unlock()

	_, exists := m.loggersPointers[name]
	return exists
}