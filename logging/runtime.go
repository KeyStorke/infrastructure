package logging

import (
	"errors"
	"fmt"
	"path"
	"runtime"
	"strings"
)

var ControlFunctionNames = []string{"LogEvent", "LogEventWithLogger"}
const MaxStackCallsForFunc = 3

// public

func CompareFunctionsNames(funcName1 string, funcName2 string) bool {
	splitted1 := strings.Split(funcName1, ".")
	splitted2 := strings.Split(funcName2, ".")

	name1 := strings.Split(funcName1, ".")[len(splitted1)-1]
	name2 := strings.Split(funcName2, ".")[len(splitted2)-1]

	result := name1 == name2

	return result
}

func FindNextFunc(funcNames []string, frames *runtime.Frames) (frame runtime.Frame, err error) {
	var matches = 0
	var matched = false

	errMsg := fmt.Sprintf("Not found functions %s: \n", funcNames)

	for {
		frame, _ = frames.Next()

		if matches == MaxStackCallsForFunc {
			return frame, err
		}

		if isLastFrameOnStack(frame) {
			err = errors.New(errMsg)
			return frame, err
		}
		for i := range funcNames {
			funcName := funcNames[i]
			if CompareFunctionsNames(frame.Func.Name(), funcName) {
				matches++
				matched = true
				break
			}
			matched = false
		}

		if matches > 0 && matched == false {
			return frame, err
		}
	}
}

func Traceback() (traceback string) {
	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])

	for {
		frame, _ := frames.Next()
		if isLastFrameOnStack(frame) {
			break
		}

		traceback += fmt.Sprintf("Frame %s:%d -> %s \n", frame.File, frame.Line, frame.Func.Name())
	}

	return traceback
}

func Line() int {
	return getFrame().Line
}

func Filepath() string {
	return getFrame().File
}

func Filename() string {
	filepath := getFrame().File
	_, filename := path.Split(filepath)
	return filename
}

func FuncName() string {
	return getFrame().Func.Name()
}

// private

func isLastFrameOnStack(frame runtime.Frame) bool {
	return frame.Line == 0
}

func getFrame() (frame runtime.Frame) {
	var err error

	pc := make([]uintptr, 15)
	n := runtime.Callers(2, pc)
	frames := runtime.CallersFrames(pc[:n])

	for {
		frame, err = FindNextFunc(ControlFunctionNames, frames)

		if err != nil {
			errMsg := fmt.Sprintf("Error: control functions %s not found, check Traceback", ControlFunctionNames)
			println(errMsg)
			println(Traceback())
			return runtime.Frame{}
		}

		return frame
	}
}