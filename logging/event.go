package logging

import (
	"errors"
	"fmt"
	"strconv"
)

type Event struct {
	Code     EventCode
	Message  string
	Priority Priority
}

type EventsMap struct {
	events map[EventCode]Event
}

func NewEventMap() EventsMap {
	m := EventsMap{}
	m.events = make(map[EventCode]Event)
	return m
}

func (m *EventsMap) GetEvent(code EventCode) (event *Event, err error) {
	e, exists := m.events[code]

	if !exists {
		hexCode := strconv.FormatInt(int64(code), 16)
		errMsg := fmt.Sprintf("not found event with code %d (aka 0x%s)", code, hexCode)
		err = errors.New(errMsg)
		return event, err
	}

	event = &e
	return event, err
}

func (m *EventsMap) AddEvent(event Event) {
	m.events[event.Code] = event
}

var CommenceEvent = Event{
	Code:     COMMENCE,
	Message:  "Commence: %FUNCNAME%",
	Priority: INFO,
}

var CompleteEvent = Event{
	Code:     COMPLETE,
	Message:  "Complete: %FUNCNAME%",
	Priority: INFO,
}
