package logging

import (
	"errors"
	"fmt"
	"io"
	"os"
	"strconv"
)

type Level struct {
	Suffix   string
	Output   io.Writer
	Priority Priority
}

type LevelsMap struct {
	levels map[Priority]Level
}

func NewLevelsMap() LevelsMap {
	m := LevelsMap{}
	m.levels = make(map[Priority]Level)
	return m
}

func (l *LevelsMap) GetLevel(priority Priority) (level *Level, err error) {
	lev, exists := l.levels[priority]

	if !exists {
		hexCode := strconv.FormatInt(int64(priority), 16)
		errMsg := fmt.Sprintf("not found level with Priority %d (aka 0x%s)", priority, hexCode)
		err = errors.New(errMsg)
		return level, err
	}

	level = &lev
	return level, err
}

func (l *LevelsMap) AddLevel(level Level) {
	l.levels[level.Priority] = level
}

var InfoLevel = Level{
	Suffix:   "[INFO] [%DAY%/%MONTH%/%YEAR% %HOUR%:%MINUTE%:%SECOND%::%MSECOND%] %FUNCNAME%->%LINENO%: ",
	Output:   os.Stdout,
	Priority: INFO,
}

var ErrorLevel = Level{
	Suffix:   "[ERROR] [%DAY%/%MONTH%/%YEAR% %HOUR%:%MINUTE%:%SECOND%::%MSECOND%] %FUNCNAME%->%LINENO%: ",
	Output:   os.Stderr,
	Priority: ERROR,
}
